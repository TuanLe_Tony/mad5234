import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTribalScreaming {
	
	/* REQ1
	 * Write a function that says 1 person is amazing.

		Function stub:
		
		scream(name)
		
		
		Example:  If name is “Peter”, the function should return this string: 
		“Peter is amazing”

	 */
	@Test
	void TestIsOneAmazingPerson() {
		// Changed the input Peter by Tony. case still passes which does not make sense ?????
		TribalScreaming oneP = new TribalScreaming();
		String result = oneP.getPeterOnly("Peter");
		assertEquals("Peter is amazing",result);
	} 
	/*
	 * When name is all uppercase letters, the method should shout at the user

		Example: If name is PETER, then function will return: 
	 */
	@Test
	void TestIsPeterShouting() {

		TribalScreaming PeterShout = new TribalScreaming();
		String result = PeterShout.getPeterOnly("PETER");
		assertEquals("PETER IS AMAZING",result);
	}
	
	/*
	 * Modify your function to accept 2 people.
		
	The people are input as an array of Strings.

	 */
	@Test
	void TestIsTwoPeopleAmazing() {
		TribalScreaming twoPeople = new TribalScreaming();
		twoPeople.push("Peter","Jigesha");
		
	}
	/*
	Modify the function to handle more than 2 people.
	Names are input as an array of strings.

	In your final output, separate each person’s name using a “comma”. The last person should have an “and” before the name.
	*/
	@Test
	void TestIsMoreThanPeopleAmazing() {
		//REQ5: green - Test case passes
		TribalScreaming twoPeople = new TribalScreaming();
		twoPeople.push("Tony","Romil","Jenelle");
		
	}
	//REQ6
	//Extend your function to handle a mixture (jumble) of uppercase and regular names.
	@Test
	void TestIsShoutingAlotOfPeople() {
	
		//REQ6: green - Test case passes but output is wrong.
		TribalScreaming twoPeople = new TribalScreaming();
		twoPeople.push("Tony","JIGESHA","Jenelle");
	}
	
	

}
